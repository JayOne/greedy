Rails.application.routes.draw do
  devise_for :users, :path_prefix => 'my'
  resources :users
  resources :results, param: :user_code
  root to: 'results#index'
  get 'admin/users', to: 'admin#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
