class CreateResults < ActiveRecord::Migration[5.2]
  def change
    create_table :results do |t|
      t.integer :clicks
      t.integer :chocolates

      t.timestamps
    end
  end
end
