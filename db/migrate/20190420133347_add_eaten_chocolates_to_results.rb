class AddEatenChocolatesToResults < ActiveRecord::Migration[5.2]
  def change
    add_column :results, :eaten_chocolates, :integer
  end
end
