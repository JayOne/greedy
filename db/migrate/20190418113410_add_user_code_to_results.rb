class AddUserCodeToResults < ActiveRecord::Migration[5.2]
  def change
    add_column :results, :user_code, :string
  end
end
