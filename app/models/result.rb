class Result < ApplicationRecord
	validates :user_code, uniqueness: true
	validates :user_code, presence: true
	def to_param
		"#{user_code}"
    end
end
