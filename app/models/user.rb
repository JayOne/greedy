class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  before_create :set_role
  enum role: [:head_admin, :admin, :user]
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  def set_role
  	self.role=2
  	self.approved=0
  end

  def last_admin?
  	@admins=User.where(role: 0)
  	@admins.count==1 and self.head_admin?
  end
end
