class UsersController < ApplicationController
	before_action :authenticate_user!

	def update
		@user= User.find(params[:id])
		if params[:approve]
			if current_user.head_admin? or (current_user.admin? and (current_user==@user))
				@user.toggle(:approved).save
			end
		elsif params[:make_admin]
			if current_user.head_admin?
				@user.update!(role: params[:make_admin])
			end
		end
		redirect_to admin_users_path				
	end
end