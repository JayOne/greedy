json.extract! result, :id, :clicks, :chocolates, :created_at, :updated_at
json.url result_url(result, format: :json)
